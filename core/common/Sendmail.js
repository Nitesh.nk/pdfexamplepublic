const nodemailer = require('nodemailer');
const logger = require('../common/logger');

async function mail(receiver, subject, text, htmllink = '') {
    // var filelocation = path;
    let mailOptions = {
        from: 'a@gmai.com',
        to: receiver,
        subject,
        text,
        html: htmllink,
        // attachments: [
        //     {
        //         filename: name+".xlsx",
        //         path: filelocation,
        //         //contents: new Buffer(data, 'base64'),
        //         //cid: cid
        //     }
        // ]
    };
    var transporter = nodemailer.createTransport({
        host: 'smtp.zoho.com', //Comment below 3 line if using Gmail
        port: 465,
        secure: true,
        // service: 'Gmail',
        auth: {
            user: 'a@gmai.com',
            pass: 'abcdef'
        }
    });
    transporter.sendMail(mailOptions).then(function (info) {
        logger.info('Mail was sent from Sendmail common');
    }).catch(function (err) {
        logger.error('Some error in sendmail common----------->', err);
    });

}
module.exports.mail = mail;

