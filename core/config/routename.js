let allroutes = {};

allroutes.routeNames = ['user'];
allroutes.userRoute = ['login', 'generatePdf'];
allroutes.authUserRoute = ['resetpwd']

module.exports = allroutes;
