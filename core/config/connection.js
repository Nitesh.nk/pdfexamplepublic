const Sequelize = require("sequelize");
const env = require("./env.js");
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  operatorsAliases: 0,
  define: {
    freezeTableName: true,
  },
  logQueryParameters: true, // log bind parameters
  // logging: false, //Loq raw query
  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle,
  },
  timezone: "+05:30",
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

function createModel() {
  let models = [
    "user", "accesscontrol", "routing", "roles", "company"
  ];

  models.forEach(function (model) {
    db[model] = require(`../../app/${model}/index`)(sequelize, Sequelize)
  });
}
createModel();

module.exports = db;
