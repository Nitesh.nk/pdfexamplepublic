module.exports = (sequelize, Sequelize) => {
	const Role = sequelize.define('mst_role', {
		role_id: {
			type: Sequelize.INTEGER(2),
			primaryKey: true,
			autoIncrement: true // Automatically gets converted to SERIAL for postgres
		},
		role_name: {
			type: Sequelize.STRING(45),
			allowNull: true
		},
	});

	return Role;
}
