module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define('mst_user', {
        user_id: {
            type: Sequelize.STRING(15),
            primaryKey: true // Automatically gets converted to SERIAL for postgres
        },
        name: {
            type: Sequelize.STRING(50),
        },
        email: {
            type: Sequelize.STRING(50),
        },
        user_phone: {
            type: Sequelize.STRING(12),
        },
        password: {
            type: Sequelize.STRING(250),
        },
        comp_id: {
            type: Sequelize.INTEGER(6),
        },
        role_id: {
            type: Sequelize.INTEGER(2),
        },
        otp: {
            type: Sequelize.STRING(6),
        },
        deviceDetail: {
            type: Sequelize.STRING(200),
        },
        fcm: {
            type: Sequelize.STRING(250),
        },
        lang_pref: {
            type: Sequelize.STRING(3),
        },
        envinfo: {
            type: Sequelize.STRING(20),
        },
        versioninfo: {
            type: Sequelize.STRING(20),
        },
        otherinfo: {
            type: Sequelize.STRING(500),
        },
        is_active: {
            type: Sequelize.INTEGER(1),
        },
        created_by: {
            type: Sequelize.STRING(50),
        },
        updated_by: {
            type: Sequelize.STRING(50),
        },
    }, {
        defaultScope: {
            attributes: {
                exclude: [
                    "createdAt",
                    "updatedAt",
                    "created_by",
                    "updated_by",
                ],
            },
        },
    }
    );

    return User;
}
