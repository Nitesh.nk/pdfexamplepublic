const userService = {};
const bcrypt = require("bcrypt");
const sequelize = require("sequelize");
const Op = sequelize.Op;
const db = require('../../core/config/connection');
const logger = require('../../core/common/logger');
const customError = require('../../core/common/errorHander');
const { user } = db;
const schemavalidate = require('./validationSchema');
const authManager = require("../auth/auth");
const Constants = require('../../Constants');
const pdf = require("pdf-creator-node");

userService.login = async function (reqObj, res) {
    try {
        const data = reqObj.body;
        const { mobile, fcm_token } = data;
        const password = data.password;
        let checkmsg = null;
        await schemavalidate.login.validate(data).catch(async (err) => {
            checkmsg = err.message;
        });
        if (checkmsg) {
            const errmsg = `In user.login error occured while yup validation ${checkmsg}`;
            let error = customError.customError(Constants.statusvalidation_code, new Error(checkmsg), errmsg, null);
            return res.json(error);
        } else {
            const userData = await db.sequelize.query(
                `select mu.user_id,mu.name,mu.email,mu.password,mu.user_phone, mu.lang_pref ,mc.comp_id,mc.comp_name,mc.comp_add,mr.role_id,mr.role_name from mst_user mu 
             ,mst_role mr,mst_comp mc where mu.comp_id=mc.comp_id and mr.role_id=mu.role_id and (mu.user_phone=:mobile or mu.email=:mobile) and mu.is_active=:is_active`,
                {
                    replacements: {
                        mobile,
                        is_active: Constants.active,
                    },
                    type: sequelize.QueryTypes.SELECT,
                }
            )

            if (userData.length > 0) {
                const correctPass = await bcrypt.compareSync(
                    password,
                    userData[0].password || "a"
                ); // bcrypt Comparing password with hashpwd
                if (correctPass) {
                    // return true if matched or else return false
                    delete userData[0].password; // Not required fields
                    const token = await authManager.generateToken(userData[0], {});
                    userData[0].token = token;
                    // if (userData[0].user_img) {
                    //   await s3img.getimgdata(userData[0].user_img.split(",")[1])
                    //     .then(result => {
                    //       userData[0].user_img = result
                    //       return result;
                    //     })
                    //     .catch(err => {
                    //       winston.error(`In user.login Some error occured for profile pic s3image ${userData[0].user_img} ------`, err);
                    //     });
                    // }

                    user.update(
                        { fcm: fcm_token },
                        {
                            where: { user_phone: mobile },
                        }
                    );
                    return res.json({ status: Constants.statusOK_code, message: Constants.statusOK_msg, data: userData[0] });

                } else {
                    const errmsg = `${mobile}  was invalid user(Password not matched)`;
                    let error = customError.customError(Constants.statusvalidation_code, new Error(Constants.Incorrectpassword_msg), errmsg, null);
                    return res.json(error);
                }
            } else {
                const errmsg = `${mobile} was invalid user(User does not exist) `;
                let error = customError.customError(Constants.statusinvaliduser_code, new Error(Constants.Invalid_user_msg), errmsg, null);
                return res.json(error);
            }
        }
    } catch (err) {
        const errmsg = `Some error occured in user.login for data ${JSON.stringify(reqObj.body)} `
        let error = customError.customError(Constants.statusissue_code, new Error(Constants.issue_msg), errmsg, err);
        return res.json(error);
    }
}

userService.resetpwd = async function (reqObj, res) {
    try {
        const { userId } = reqObj;
        const { password, newPassword } = reqObj.body;
        let checkmsg = null;

        await schemavalidate.resetPwd.validate(reqObj.body)
            .catch(async (err) => {
                checkmsg = err.message;
            });
        if (checkmsg) {
            const errmsg = `In user.resetPwd error occured while yup validation ${checkmsg} `;
            let error = customError.customError(Constants.statusvalidation_code, new Error(checkmsg), errmsg, null);
            return res.json(error);
        } else {
            let userexist = await user.findOne({
                where: {
                    [Op.and]: [
                        { user_id: userId }, { is_active: { [Op.ne]: Constants.inactive } },
                    ],
                },
            })
            if (userexist) {
                const result = await bcrypt.compareSync(password, userexist.password);
                if (result) {
                    const hash = bcrypt.hashSync(newPassword, Constants.saltRounds);
                    await user.update({ password: hash }, { where: { user_id: userId } })
                        .then(async (updatestatus) => {
                            if (updatestatus[0] > 0) {
                                return res.json({ status: Constants.statusOK_code, message: Constants.statusOK_msg });
                            } else {
                                const errmsg = `In user.resetpwd password was not updated for user for user: ${userId}`;
                                let error = customError.customError(Constants.statusinvalidinput_code, new Error(Constants.invalid_input_msg), errmsg, null);
                                return res.json(error);
                            }
                        })
                } else {
                    const errmsg = `In user.resetPwd data not available for user ${reqObj.userId}----  `;
                    let error = customError.customError(Constants.statusinvalidinput_code, new Error(Constants.oldPassIncorrect), errmsg, null);
                    return res.json(error);
                }
            } else {
                const errmsg = `In user.resetPwd incorrect old password was passed by user ${userId} ----  `;
                let error = customError.customError(Constants.statusinvalidinput_code, new Error(Constants.Incorrectpassword_msg), errmsg, null);
                return res.json(error);
            }

        }
    } catch (err) {
        const errmsg = `Some error occured in user.resetpwd for user ${reqObj.userId} `
        let error = customError.customError(Constants.statusissue_code, new Error(Constants.issue_msg), errmsg, err);
        return res.json(error);
    }
}

userService.generatePdf = async function (reqObj, res) {
    try {
        let document = {
            html: Constants.pdfHtml,
            data: {
                header: "Pdf Generated from BE"
            },
            path: `${ROOT_DIR}/pdfloc/SamplePdf.pdf`
        };

        let pdfResponse = await pdf.create(document, {
            format: "A3",
            orientation: "landscape",
            "width": "25in"
        })
        console.log('====================================');
        console.log(pdfResponse);
        console.log('====================================');
        return res.json({ status: Constants.statusOK_code, message: Constants.statusOK_msg });
    } catch (err) {
        const errmsg = `Some error occured in user.generatePdf for user ${reqObj.userId || 0} `
        let error = customError.customError(Constants.statusissue_code, new Error(Constants.issue_msg), errmsg, err);
        return res.json(error);
    }
}

module.exports = userService;
