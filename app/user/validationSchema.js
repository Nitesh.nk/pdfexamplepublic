const yup = require('yup');
const Constants = require('../../Constants');

const schemavalidate = {};

schemavalidate.login = yup.object().shape({
  mobile: yup.number()
    .typeError(`Mobile${Constants.number_error}`)
    .min(1000000000, Constants.mobileLength_error)
    .max(9999999999, Constants.mobileLength_error)
    .required(`Mobile${Constants.required_error}`),
  password: yup.string()
    .trim()
    .required(`Password${Constants.required_error}`)
    .min(Constants.passLength, Constants.passLength_error),
});

schemavalidate.resetPwd = yup.object().shape({
  password: yup.string()
    .trim()
    .required(`Password${Constants.required_error}`)
    .min(Constants.passLength, Constants.passLength_error),
  newPassword: yup.string()
    .trim()
    .required(`New Password${Constants.required_error}`)
    .min(Constants.passLength, Constants.passLength_error),
});
module.exports = schemavalidate;
