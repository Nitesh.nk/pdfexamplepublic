module.exports = (sequelize, Sequelize) => {
  const Routing = sequelize.define('mst_comp', {
    comp_id: {
      type: Sequelize.INTEGER(6),
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    comp_add: {
      type: Sequelize.STRING(150),
      allowNull: false
    },
    comp_name: {
      type: Sequelize.STRING(50),
      allowNull: false
    },
    logo_url: {
      type: Sequelize.STRING(500),
    },
    sub_type: {
      type: Sequelize.STRING(20),
      defaultValue: "Free"
    },
    template_url: {
      type: Sequelize.STRING(500),
    },
    is_active: {
      type: Sequelize.INTEGER(1),
    },
    created_by: {
      type: Sequelize.STRING(50),
      defaultValue: "admin"
    },
    updated_by: {
      type: Sequelize.STRING(50),
    }
  });

  return Routing;
};
